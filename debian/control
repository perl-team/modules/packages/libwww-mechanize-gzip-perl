Source: libwww-mechanize-gzip-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Florian Schlichting <fsfs@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: perl,
                     libwww-mechanize-perl
Standards-Version: 4.2.1
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libwww-mechanize-gzip-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libwww-mechanize-gzip-perl.git
Homepage: https://metacpan.org/release/WWW-Mechanize-GZip

Package: libwww-mechanize-gzip-perl
Architecture: all
Depends: ${perl:Depends},
         ${misc:Depends},
         libwww-mechanize-perl
Description: Perl module to fetch webpages with gzip-compression
 The WWW::Mechanize::GZip module tries to fetch a URL by requesting
 gzip-compression from the webserver.
 .
 If the response contains a header with 'Content-Encoding: gzip', it
 decompresses the response in order to get the original (uncompressed)
 content.
 .
 This module will help to reduce bandwidth fetching webpages, if supported
 by the webeserver. If the webserver does not support gzip-compression,
 no compression will be used.
 .
 This modules is a direct subclass of WWW::Mechanize and will therefore
 support any methods provided by WWW::Mechanize.
 .
 The decompression is handled by Compress::Zlib::memGunzip.
